<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\general\LihuaCity;
use app\common\model\Version;
use OSS\Core\OssException;
use OSS\OssClient;

/**
 * 公共接口
 */
class Common extends Api
{
    protected $noNeedLogin = ['init', 'uppic'];
    protected $noNeedRight = '*';
    protected $_err = '';

    /**
     * 加载初始化
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="version", type="string", required=true, description="版本号, 如：1.1.1")
     * @ApiReturn   ({"code":1,"msg":"","time":"1571643206","data":{"versiondata":{"enforce":1,"version":"0.0.9","newversion":"1.0.0","downloadurl":"https:\/\/www.baidu.com","packagesize":"20M","upgradetext":"更新内容"},"city_list":[{"id":3,"name":"常州","store_list":[{"id":17,"name":"鸣新","addr":"","lng":null,"lat":null},{"id":18,"name":"湖塘","addr":"","lng":null,"lat":null},{"id":27,"name":"火车站","addr":"","lng":null,"lat":null},{"id":28,"name":"城北","addr":"常州市新北区太湖中路71号附近","lng":"31.822805","lat":"119.974824"}]},{"id":4,"name":"南京","store_list":[{"id":19,"name":"白下路","addr":"","lng":null,"lat":null},{"id":21,"name":"瑞金路","addr":"","lng":null,"lat":null},{"id":24,"name":"虹桥","addr":"","lng":"0.000000","lat":"0.000000"},{"id":26,"name":"徐庄","addr":"","lng":null,"lat":null},{"id":29,"name":"黄山路","addr":"","lng":null,"lat":null}]}]}})
     */
    public function init()
    {
        if ($version = $this->request->request('version')) {
            $content = [
                'versiondata' => Version::check($version),
                'city_list'   => LihuaCity::getCityAndStoreList(),
            ];
            $this->success('OK', $content);
        } else {
            $this->error('参数错误');
        }
    }

    /**
     * 上传图片
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="group", type="string", required=false, description="图片分组，点评为 comment")
     * @ApiParams   (name="file", type="File", required=true, description="文件流")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1571751137","data":{"url":"\/uploads\/comment\/2019\/10\/87915daf04e184df7.jpg"}})
     */
    public function uppic()
    {
        $group = $this->request->post('group', 'default');
        $f = $this->request->file('file');
        if (empty($f)) $this->error('请选择要上传的图片');

        $uploadDir = '/uploads/'. $group .'/'.date('Y/m/');
        $fileInfo  = $f->getInfo();
        $suffix    = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        if (!in_array($suffix, ['jpg','jpeg','png','gif'])) $this->error('图片格式不正确');

        $uniqid    = uniqid(mt_rand(1000,9999));
        $fileName  = $uniqid . '.' . $suffix;
        $picUrl    = $uploadDir . $fileName;
        $picPath   = ROOT_PATH . 'public' . $picUrl;
        $splInfo   = $f->move(ROOT_PATH . 'public' . $uploadDir, $fileName);
        if ($splInfo) {
            switch ($group) {
                case 'comment':
                    $bigPicUrl = $uploadDir . $uniqid . '_big.' . $suffix;
                    copy(ROOT_PATH . 'public' . $picUrl, ROOT_PATH . 'public' . $bigPicUrl);
                    $img = \think\Image::open($picPath);
                    $img->thumb(400, 400, 3)->save($picPath, null, 85);
                    $bigImg = \think\Image::open(ROOT_PATH . 'public' . $bigPicUrl);
                    $bigImg->thumb(1200, 1200)->save(ROOT_PATH . 'public' . $bigPicUrl);
                    $ret = $this->upOss($bigPicUrl);
                    if (!$ret) $this->error($this->_err);
                    $ret = $this->upOss($picUrl);
                    break;

                default:
                    $img = \think\Image::open($picPath);
                    $img->thumb(1200, 1200)->save($picPath);
                    $ret = $this->upOss($picUrl);
                    break;
            }
            if (!$ret) $this->error($this->_err);

        } else {
            $this->error($f->getError());
        }

        $this->success('OK', ['url' => $picUrl]);
    }

    /**
     * 上传文件至阿里oss
     *
     * @param $file
     * @return bool
     */
    protected function upOss($file)
    {
        $keyId     = "LTAI4FbtLusx8NJ78YreYW54";
        $keySecret = "24Ma12hXsg12ERcNG02cLZH0nG4uUb";
        $endpoint  = "http://oss-cn-shanghai.aliyuncs.com";
        $bucket    = "wshtest";
        try {
            $ossClient = new OssClient($keyId, $keySecret, $endpoint);
            $ossClient->uploadFile($bucket, ltrim($file, '/'), ROOT_PATH . 'public' . $file);
        }
        catch (OssException $e) {
            $this->_err = $e->getMessage();
            return false;
        }
        return true;
    }

}
