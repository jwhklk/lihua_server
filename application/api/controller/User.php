<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\User as UserModel;
use app\common\library\Sms;
use fast\Random;
use think\Validate;

/**
 * 会员接口
 */
class User extends Api
{
    protected $noNeedLogin = ['login', 'mobilelogin', 'register', 'resetpwd', 'third'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 会员中心
     */
    public function index()
    {
        $this->success('', ['welcome' => $this->auth->nickname]);
    }

    /**
     * 会员登录
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="account", type="string", required=true, description="用户名或手机号")
     * @ApiParams   (name="password", type="string", required=true, description="密码")
     * @ApiReturn   ({"code":1,"msg":"登录成功","time":"1569515572","data":{}})
     */
    public function login()
    {
        $account  = $this->request->request('account');
        $password = $this->request->request('password');
        if (!$account || !$password) $this->error('参数错误');

        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success('登录成功', $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 手机验证码登录
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="mobile", type="string", required=true, description="手机号")
     * @ApiParams   (name="captcha", type="string", required=true, description="验证码")
     * @ApiReturn   ({"code":1,"msg":"登录成功","time":"1569515572","data":{}})
     */
    public function mobilelogin()
    {
        $mobile  = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) $this->error('参数错误');

        if (!Validate::regex($mobile, "^1\d{10}$")) $this->error('手机号格式错误');
        if (!Sms::check($mobile, $captcha, 'login')) $this->error('验证码错误');

        $user = UserModel::getByMobile($mobile);
        if ($user) {
            if ($user->status != 'normal') $this->error(__('Account is locked'));
            //如果已经有账号则直接登录
            $ret = $this->auth->direct($user->id);
        } else {
            $ret = $this->auth->register($mobile, Random::alnum(), '', $mobile, []);
        }
        if ($ret) {
            Sms::flush($mobile, 'login');
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success('登录成功', $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注册会员
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="username", type="string", required=true, description="用户名")
     * @ApiParams   (name="password", type="string", required=true, description="密码")
     * @ApiParams   (name="mobile", type="string", required=true, description="手机号")
     * @ApiReturn   ({"code":1,"msg":"注册成功","time":"1569515572","data":{}})
     */
    public function register()
    {
        $username = $this->request->request('username');
        $password = $this->request->request('password');
        $mobile   = $this->request->request('mobile');
        if (!$username || !$password) $this->error('参数错误');

        if ($mobile && !Validate::regex($mobile, "^1\d{10}$")) $this->error('手机号格式错误');

        $ret = $this->auth->register($username, $password, '', $mobile, []);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success('注册成功', $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注销登录
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiReturn   ({"code":1,"msg":"注销成功","time":"1569515572","data":{}})
     */
    public function logout()
    {
        $this->auth->logout();
        $this->success('注销成功');
    }

    /**
     * 修改用户名
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="username", type="string", required=true, description="用户名")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1569515572","data":{"username":"hardy"}})
     */
    public function changeusername()
    {
        $username = $this->request->post('username');
        if ($username) {
            if (!Validate::is($username, 'alphaNum')) $this->error('用户名只允许字母及数字');
            if (mb_strlen($username) > 30) $this->error('用户名只允许30字以内');
            $exists = UserModel::where('username', $username)->where('id', '<>', $this->auth->id)->find();
            if ($exists) $this->error('用户名已存在');

            UserModel::update(['username' => $username], ['id' => $this->auth->id]);
            $this->success('OK', ['username' => $username]);
        }
        $this->error('修改失败');
    }

    /**
     * 修改昵称
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="nickname", type="string", required=true, description="昵称")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1569515572","data":{"nickname":"大帝"}})
     */
    public function changenickname()
    {
        $nickname = $this->request->post('nickname');
        if ($nickname) {
            if (!Validate::is($nickname, 'chsAlphaNum')) $this->error('昵称只允许汉字、字母和数字');
            if (mb_strlen($nickname) > 15) $this->error('昵称只允许15字以内');

            UserModel::update(['nickname' => $nickname], ['id' => $this->auth->id]);
            $this->success('OK', ['nickname' => $nickname]);
        }
        $this->error('修改失败');
    }

    /**
     * 修改手机号
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="mobile", type="string", required=true, description="手机号")
     * @ApiParams   (name="captcha", type="string", required=true, description="验证码")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1569515572","data":{"mobile":"13961259888"}})
     */
    public function changemobile()
    {
        $user    = $this->auth->getUser();
        $mobile  = $this->request->post('mobile');
        $captcha = $this->request->post('captcha');
        if (!$mobile || !$captcha) $this->error('参数错误');

        if (!Validate::regex($mobile, "^1\d{10}$")) $this->error('手机号格式错误');

        $exists = UserModel::where('mobile', $mobile)->where('id', '<>', $user->id)->find();
        if ($exists) $this->error('手机号已存在');

        $res = Sms::check($mobile, $captcha, 'changemobile');
        if (!$res) $this->error('验证码错误');

        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'changemobile');
        $this->success('OK', ['mobile' => $mobile]);
    }

    /**
     * 设置密码
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="newpwd", type="string", required=true, description="新密码")
     * @ApiParams   (name="confirmpwd", type="string", required=true, description="确认密码")
     * @ApiReturn   ({"code":1,"msg":"设置密码成功","time":"1569515572","data":{}})
     */
    public function changepwd()
    {
        $newpwd  = $this->request->post("newpwd");
        $confirmpwd  = $this->request->post("confirmpwd");
        if (!$newpwd || !$confirmpwd) $this->error('参数错误');

        if ($newpwd != $confirmpwd) $this->error('两次密码不一致');
        if (mb_strlen($newpwd) < 6) $this->error('密码长度至少6位');

        $ret = $this->auth->changepwd($newpwd, '', true);
        if ($ret) {
            $this->success('设置密码成功');
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 重置密码
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="mobile", type="string", required=true, description="手机号")
     * @ApiParams   (name="newpwd", type="string", required=true, description="新密码")
     * @ApiParams   (name="captcha", type="string", required=true, description="验证码")
     * @ApiReturn   ({"code":1,"msg":"重置密码成功","time":"1569515572","data":{}})
     */
    public function resetpwd()
    {
        $mobile  = $this->request->request("mobile");
        $newpwd  = $this->request->request("newpwd");
        $captcha = $this->request->request("captcha");
        if (!$newpwd || !$captcha) $this->error('参数错误');

        if (!Validate::regex($mobile, "^1\d{10}$")) $this->error('手机号格式错误');
        if (mb_strlen($newpwd) < 6) $this->error('密码长度至少6位');

        $user = UserModel::getByMobile($mobile);
        if (!$user) $this->error('用户不存在');

        $ret = Sms::check($mobile, $captcha, 'login');
        if (!$ret) $this->error('验证码错误');

        Sms::flush($mobile, 'login');

        //模拟一次登录
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpwd, '', true);
        if ($ret) {
            $this->success('重置密码成功');
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 第三方登录
     *
     * @ApiMethod   (POST)
     * @ApiParams   (name="platform", type="string", required=true, description="平台名称")
     * @ApiParams   (name="code", type="string", required=true, description="Code码")
     */
    public function third()
    {
        $url = url('user/index');
        $platform = $this->request->request("platform");
        $code = $this->request->request("code");
        $config = get_addon_config('third');
        if (!$config || !isset($config[$platform])) {
            $this->error(__('Invalid parameters'));
        }
        $app = new \addons\third\library\Application($config);
        //通过code换access_token和绑定会员
        $result = $app->{$platform}->getUserInfo(['code' => $code]);
        if ($result) {
            $loginret = \addons\third\library\Service::connect($platform, $result);
            if ($loginret) {
                $data = [
                    'userinfo'  => $this->auth->getUserinfo(),
                    'thirdinfo' => $result
                ];
                $this->success(__('Logged in successful'), $data);
            }
        }
        $this->error(__('Operation failed'), $url);
    }

}
