<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\LihuaUserinvo;
use think\Exception;


/**
 * 发票信息
 */
class Userinvo extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 发票信息列表
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1567154614","data":{"list":[{"id":1,"type":2,"title":"大唐科技","taxno":"123456789012345","type_text":"单位"},{"id":2,"type":1,"title":"大帝","taxno":"","type_text":"个人"}]}})
     */
    public function index()
    {
        $list = LihuaUserinvo::all(function ($query) {
            $query->where('user_id', $this->auth->id)->field('user_id', true);
        });
        $this->success('OK', ['list' => $list]);
    }

    /**
     * 查看发票信息
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="string", required=true, description="发票信息id")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1567155141","data":{"id":1,"user_id":3,"type":2,"title":"象与科技","taxno":"123456789012345","type_text":"单位"}})
     */
    public function info()
    {
        $id = $this->request->post('id');
        if (!$id) $this->error('参数错误');

        $row = LihuaUserinvo::where(['id'=>$id, 'user_id'=>$this->auth->id])->find();
        if (!$row) $this->error('非法访问');

        $this->success('OK', $row);
    }

    /**
     * 新增发票信息
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="type", type="int", required=true, description="类型：1=个人,2=单位")
     * @ApiParams   (name="title", type="string", required=true, description="抬头")
     * @ApiParams   (name="taxno", type="string", required=true, description="税号，单位必须填写")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1567156555","data":{"id":4,"user_id":3,"type":2,"title":"联想科技","taxno":"123321456654789987","type_text":"单位"}})
     */
    public function add()
    {
        $type  = $this->request->post('type');
        $title = $this->request->post('title', '');
        $taxno = $this->request->post('taxno', '');

        if (!in_array($type, [1, 2])) $this->error('请选择类型');
        if (!$title) $this->error('请填写抬头');
        if ($type == 2 && !$taxno) $this->error('请填写税号');

        try {
            $invo = LihuaUserinvo::create([
                'user_id' => $this->auth->id,
                'type'    => $type,
                'title'   => $title,
                'taxno'   => $type == 2 ? $taxno : '',
            ]);
        } catch (Exception $e) {
            $this->error('填写有误');
        }

        $this->success('OK', LihuaUserinvo::get($invo->id));
    }

    /**
     * 编辑发票信息
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="string", required=true, description="发票信息id")
     * @ApiParams   (name="type", type="int", required=true, description="类型：1=个人,2=单位")
     * @ApiParams   (name="title", type="string", required=true, description="抬头")
     * @ApiParams   (name="taxno", type="string", required=true, description="税号，单位必须填写")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1567156428","data":{"id":1,"user_id":3,"type":1,"title":"张三丰","taxno":"","type_text":"个人"}})
     */
    public function edit()
    {
        $id    = $this->request->post('id');
        $type  = $this->request->post('type', 0);
        $title = $this->request->post('title', '');
        $taxno = $this->request->post('taxno', '');

        if (!$id) $this->error('参数错误');
        if (!in_array($type, [1, 2])) $this->error('请选择类型');
        if (!$title) $this->error('请填写抬头');
        if ($type == 2 && !$taxno) $this->error('请填写税号');

        try {
            LihuaUserinvo::update([
                'type'    => $type,
                'title'   => $title,
                'taxno'   => $type == 2 ? $taxno : '',
            ], ['id' => $id, 'user_id' => $this->auth->id]);
        } catch (Exception $e) {
            $this->error('填写有误');
        }

        $this->success('OK', LihuaUserinvo::get($id));
    }

    /**
     * 删除发票信息
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="string", required=true, description="发票信息id")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1566500187","data":{}})
     */
    public function del()
    {
        $id = $this->request->post('id');
        if (!$id) $this->error('参数错误');

        $ret = LihuaUserinvo::where(['id' => $id, 'user_id' => $this->auth->id])->delete();
        if (!$ret) $this->error('删除失败');

        $this->success('OK');
    }

}
