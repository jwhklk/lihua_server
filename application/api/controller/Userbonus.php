<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\LihuaUserbonus;

/**
 * 我的红包
 */
class Userbonus extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 红包列表
     *
     * @ApiMethod   (POST)
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="city_id", type="int", required=true, description="城市ID")
     * @ApiParams   (name="type", type="int", required=true, description="1有效，2失效，默认为1")
     * @ApiParams   (name="page", type="int", required=true, description="当前页，默认第1页")
     * @ApiParams   (name="page_size", type="int", required=false, description="每页条数，默认20条")
     * @ApiReturn   ({"code":1,"msg":"OK","time":"1569220820","data":{"page":1,"total":13,"page_size":10,"total_page":2,"list":[{"id":16,"name":"5元红包","type":2,"reach":25,"reduce":5,"des":"满25元减5元","start_date":"2019-09-21","end_date":"2019-10-19"},{"id":15,"name":"3元红包","type":3,"reach":0,"reduce":3,"des":"无门槛限制","start_date":"2019-09-21","end_date":"2019-11-30"},{"id":14,"name":"天降红包","type":3,"reach":0,"reduce":8,"des":"无门槛限制","start_date":"2019-09-21","end_date":"2019-10-30"},{"id":13,"name":"10元红包","type":2,"reach":45,"reduce":10,"des":"满45元减10元","start_date":"2019-09-21","end_date":"2019-11-30"},{"id":12,"name":"开心转盘红包","type":3,"reach":0,"reduce":5,"des":"无门槛限制","start_date":"2019-09-21","end_date":"2019-10-28"},{"id":11,"name":"10元通用红包","type":2,"reach":30,"reduce":10,"des":"满30元减10元","start_date":"2019-09-21","end_date":"2019-11-22"},{"id":10,"name":"天降红包","type":2,"reach":40,"reduce":7,"des":"满40元减7元","start_date":"2019-09-21","end_date":"2019-11-01"},{"id":8,"name":"5元红包","type":3,"reach":0,"reduce":5,"des":"无门槛限制","start_date":"2019-09-21","end_date":"2019-11-15"},{"id":6,"name":"8元红包","type":2,"reach":35,"reduce":8,"des":"满35元减8元","start_date":"2019-09-21","end_date":"2019-11-30"},{"id":5,"name":"5元通用红包","type":2,"reach":25,"reduce":5,"des":"满25元减5元","start_date":"2019-09-21","end_date":"2019-11-18"}]}})
     */
    public function index()
    {
        $city_id   = (int)$this->request->post('city_id');
        if (!$city_id) $this->error('参数错误');
        $type      = (int)$this->request->post('type', 1);
        $page      = max(1, (int)$this->request->post('page'));
        $page_size = (int)$this->request->post('page_size') ? : 20;
        $data      = [
            'page'       => $page,
            'total'      => 0,
            'page_size'  => $page_size,
            'total_page' => 0,
            'list'       => []
        ];

        LihuaUserbonus::updateExpireBonusStatus($this->auth->id);//更新失效红包状态

        $where['user_id'] = $this->auth->id;
        $where['city_id'] = ['in', [0, $city_id]];
        if ($type == 1) {
            $where['status'] = 0;
        } else {
            $where['status'] = ['>', 0];
        }
        $total = LihuaUserbonus::where($where)->count();
        if ($total) {
            $data['total']      = $total;
            $data['total_page'] = ceil($total/$page_size);
            $list = LihuaUserbonus::where($where)
                ->field('id,name,type,reach,reduce,des,start_date,end_date')
                ->order('id', 'desc')->page($page, $page_size)->select();
            foreach ($list as $k => $v) {
                $list[$k]['reach']  = round($v['reach'], 2);
                $list[$k]['reduce'] = round($v['reduce'], 2);
                if (!$list[$k]['des']) {
                    if ($list[$k]['reach'] == 0) {
                        $list[$k]['des'] = '无门槛限制';
                    } else {
                        $list[$k]['des'] = '满' . $list[$k]['reach'] . '元减' . $list[$k]['reduce'] . '元';
                    }
                }
            }
            $data['list'] = $list;
        }

        $this->success('OK', $data);
    }


}
