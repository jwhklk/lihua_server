<?php

namespace app\api\behavior;

use app\common\model\LihuaToolsConf;
use app\common\model\LihuaUserbonus;

class XinrenHongbao
{
    public function run(&$params, $data = [])
    {
        $row = LihuaToolsConf::where('type', 2)
            ->where('status', 1)->find();
        if ($row && $row['config'] && (!$row['end_date'] || $row['end_date'] >= date('Y-m-d'))) {
            $configList = json_decode($row['config'], true);
            foreach ($configList as $k => $v) {
                $v['type'] = 1;//1新人红包 2满减 3无门槛
                $v['user_id'] = $params['id'];
                LihuaUserbonus::addUserBonus($v);
            }
        }
    }
}
