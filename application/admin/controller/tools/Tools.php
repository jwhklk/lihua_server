<?php

namespace app\admin\controller\tools;

use app\common\controller\Backend;
use app\common\model\LihuaToolsConf;


/**
 * 营销工具
 *
 * @icon fa fa-share-alt
 */
class Tools extends Backend
{
    protected $noNeedLogin = [];
    protected $noNeedRight = [];

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 工具首页
     */
    public function index()
    {
        return $this->view->fetch();
    }

    /**
     * 订单满减
     */
    public function enough()
    {
        if ($this->request->isAjax()) {
            $row = $this->request->post('row/a');
            if (!$row['end_date']) $row['end_date'] = null;
            if ($row['id'] > 0) {
                LihuaToolsConf::update($row);
            } else {
                $row['city_id'] = $this->auth->city_id;
                $row['type'] = 1;
                LihuaToolsConf::create($row);
            }
            $this->success();
        }

        if ($this->auth->isSuperAdmin() || !$this->auth->city_id) {
            $this->error('仅城市管理员可操作', '');
        }

        $row = LihuaToolsConf::where(['city_id' => $this->auth->city_id, 'type' => 1])->find();
        $this->assign('row', $row);
        return $this->view->fetch();
    }

    /**
     * 新人红包
     */
    public function xinren()
    {
        if ($this->request->isAjax()) {
            $row = $this->request->post('row/a');
            if (!$row['end_date']) $row['end_date'] = null;
            if ($row['id'] > 0) {
                LihuaToolsConf::update($row);
            } else {
                $row['type'] = 2;
                LihuaToolsConf::create($row);
            }
            $this->success();
        }

        if (!$this->auth->isSuperAdmin()) $this->error('无权操作', '');

        $row = LihuaToolsConf::where('type', 2)->find();
        $this->assign('row', $row);
        return $this->view->fetch();
    }

}


