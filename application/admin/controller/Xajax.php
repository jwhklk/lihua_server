<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use OSS\OssClient;
use OSS\Core\OssException;
use think\Config;

/**
 * Xajax异步请求接口
 * @internal
 */
class Xajax extends Backend
{
    protected $noNeedRight = ['*'];
    protected $layout = '';
    protected $_err = '';

    public function _initialize()
    {
        parent::_initialize();

        //设置过滤方法
        $this->request->filter(['strip_tags', 'htmlspecialchars']);
    }

    /**
     * 上传图片
     */
    public function uppic($group = 'default')
    {
        Config::set('default_return_type', 'json');
        $f = $this->request->file('file');
        if (empty($f)) $this->error('请选择图片');

        $uploadDir = '/uploads/'. $group .'/'.date('Y/m/');
        $fileInfo  = $f->getInfo();
        $suffix    = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        if (!in_array($suffix, ['jpg','jpeg','png','gif'])) $this->error('图片格式不正确');

        $uniqid    = uniqid(mt_rand(1000,9999));
        $fileName  = $uniqid . '.' . $suffix;
        $picUrl    = $uploadDir . $fileName;
        $picPath   = ROOT_PATH . 'public' . $picUrl;
        $splInfo   = $f->move(ROOT_PATH . 'public' . $uploadDir, $fileName);
        if ($splInfo) {
            switch ($group) {
                case 'goods':
                    $bigPicUrl = $uploadDir . $uniqid . '_big.' . $suffix;
                    copy(ROOT_PATH . 'public' . $picUrl, ROOT_PATH . 'public' . $bigPicUrl);
                    $img = \think\Image::open($picPath);
                    $img->thumb(400, 400, 3)->save($picPath, null, 85);
                    $bigImg = \think\Image::open(ROOT_PATH . 'public' . $bigPicUrl);
                    $bigImg->thumb(1200, 1200)->save(ROOT_PATH . 'public' . $bigPicUrl, null, 85);
                    $ret = $this->upOss($bigPicUrl);
                    if (!$ret) $this->error($this->_err);
                    $ret = $this->upOss($picUrl);
                    break;

                case 'slide':
                    $img = \think\Image::open($picPath);
                    $img->thumb(800, 320, 3)->save($picPath, null, 85);
                    $ret = $this->upOss($picUrl);
                    break;

                case 'score':
                    $img = \think\Image::open($picPath);
                    $img->thumb(400, 400, 3)->save($picPath);
                    $ret = $this->upOss($picUrl);
                    break;

                default:
                    $img = \think\Image::open($picPath);
                    $img->thumb(1200, 1200)->save($picPath);
                    $ret = $this->upOss($picUrl);
                    break;
            }
            if (!$ret) $this->error($this->_err);

        } else {
            $this->error($f->getError());
        }

        $this->success('上传成功', null, ['url' => $picUrl]);
    }

    protected function upOss($file)
    {
        $keyId     = "LTAI4FbtLusx8NJ78YreYW54";
        $keySecret = "24Ma12hXsg12ERcNG02cLZH0nG4uUb";
        $endpoint  = "http://oss-cn-shanghai.aliyuncs.com";
        $bucket    = "wshtest";
        try {
            $ossClient = new OssClient($keyId, $keySecret, $endpoint);
            $ossClient->uploadFile($bucket, ltrim($file, '/'), ROOT_PATH . 'public' . $file);
        }
        catch (OssException $e) {
            $this->_err = $e->getMessage();
            return false;
        }
        return true;
    }
}
