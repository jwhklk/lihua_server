<?php

namespace app\admin\controller\general;

use app\common\controller\Backend;

/**
 * 门店管理
 *
 * @icon fa fa-circle-o
 */
class LihuaStore extends Backend
{
    protected $cityLimit = true;
    protected $noNeedRight = ['selpoint', 'selarea'];

    /**
     * LihuaStore模型对象
     * @var \app\common\model\general\LihuaStore
     */
    protected $model = null;

    public function _initialize()
    {
        $action = $this->request->action();
        if ($action == 'selpoint' || $action == 'selarea') $this->layout = false;
        parent::_initialize();
        $this->model = new \app\common\model\general\LihuaStore;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 查看
     */
    public function index()
    {
        $this->relationSearch = true;
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $total = $this->model
                ->with(['lihuacity'])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['lihuacity'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {

                $row->getRelation('lihuacity')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 选取门店坐标
     */
    public function selpoint()
    {
        $this->request->filter(['strip_tags']);
        $city_id = $this->request->get('city_id');
        $lng  = $this->request->get('lng');
        $lat  = $this->request->get('lat');
        $city = '';
        if ($city_id) {
            $city = \app\common\model\general\LihuaCity::where('id', $city_id)->value('name');
        }

        $this->assign(['city' => $city, 'lng' => $lng, 'lat' => $lat]);
        return $this->fetch();
    }

    /**
     * 选取配送范围
     */
    public function selarea()
    {
        $this->request->filter(['strip_tags']);
        $store_id = $this->request->get('store_id');
        if ($store_id > 0) {
            $row = $this->model->where('id', $store_id)->field('lng,lat,area')->find();
        } else {
            $lng = $this->request->get('lng');
            $lat = $this->request->get('lat');
            $row = ['lng' => $lng, 'lat' => $lat, 'area' => null];
        }
        $this->assign('row', $row);
        return $this->fetch();
    }
}
