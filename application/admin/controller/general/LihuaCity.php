<?php

namespace app\admin\controller\general;

use app\common\controller\Backend;

/**
 * 城市管理
 *
 * @icon fa fa-circle-o
 */
class LihuaCity extends Backend
{
    protected $cityLimit = true;
    protected $cityLimitField = 'id';
    protected $noNeedRight = ['sel'];

    /**
     * LihuaCity模型对象
     * @var \app\common\model\general\LihuaCity
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\general\LihuaCity;
        $this->view->assign('payTypeList', $this->model::getPayTypeList());
    }

    /**
     * 选择城市
     *
     * @internal
     */
    public function sel()
    {
        $list = $this->model::getCityList();
        if ($this->auth->city_id > 0) {
            foreach ($list as $k => $v) {
                if ($v['id'] != $this->auth->city_id) unset($list[$k]);
            }
        }
        return json($list);
    }
}
