<?php

namespace app\admin\controller\general;

use app\common\controller\Backend;
use think\Db;

/**
 * 支付设置
 *
 * @icon fa fa-circle-o
 */
class Payset extends Backend
{

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 小程序支付
     */
    public function xcx()
    {
        if ($this->request->isAjax())
        {
            $row = $this->request->post('row/a');
            $row['type'] = 2;

            $plt = $this->request->post('plt');
            $table = $plt == 1 ? 'lihua_alipay' : 'lihua_wxpay';

            if ($row['id'] > 0) {
                Db::name($table)->update($row);
            } else {
                unset($row['id']);
                Db::name($table)->insert($row);
            }

            $this->success();
        }

        $zfb = Db::name('lihua_alipay')->where(['city_id' => 0, 'type' => 2])->find();
        $wx  = Db::name('lihua_wxpay')->where(['city_id' => 0, 'type' => 2])->find();

        $this->view->assign(['zfb' => $zfb, 'wx' => $wx]);
        return $this->view->fetch();
    }

    /**
     * 支付宝支付
     */
    public function alipay($city_id = 0)
    {
        if ($this->auth->city_id > 0) $city_id = $this->auth->city_id;

        if ($this->request->isAjax())
        {
            $row = $this->request->post('row/a');
            if ($row['id'] > 0) {
                Db::name('lihua_alipay')->update($row);
            } else {
                unset($row['id']);
                $row['city_id'] = $city_id;
                $row['type'] = 1;
                Db::name('lihua_alipay')->insert($row);
            }

            $this->success();
        }

        $row = Db::name('lihua_alipay')->where(['city_id' => $city_id, 'type' => 1])->find();

        $this->view->assign('row', $row);
        return $this->view->fetch();
    }

    /**
     * 微信支付
     */
    public function wxpay($city_id = 0)
    {
        if ($this->auth->city_id > 0) $city_id = $this->auth->city_id;

        if ($this->request->isAjax())
        {
            $row = $this->request->post('row/a');
            if ($row['id'] > 0) {
                Db::name('lihua_wxpay')->update($row);
            } else {
                unset($row['id']);
                $row['city_id'] = $city_id;
                $row['type'] = 1;
                Db::name('lihua_wxpay')->insert($row);
            }

            $this->success();
        }

        $row = Db::name('lihua_wxpay')->where(['city_id' => $city_id, 'type' => 1])->find();

        $this->view->assign('row', $row);
        return $this->view->fetch();
    }
}
