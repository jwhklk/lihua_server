<?php

namespace app\admin\controller\order;

use app\common\controller\Backend;
use app\common\model\LihuaOrderGoods;

/**
 * 订单管理
 *
 * @icon fa fa-circle-o
 */
class Order extends Backend
{
    protected $cityLimit = true;

    /**
     * Order模型对象
     * @var \app\common\model\order\Order
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\order\Order;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->where($where)->order($sort, $order)->count();
            $list  = $this->model->where($where)->order($sort, $order)->limit($offset, $limit)->select();
            $list  = collection($list)->toArray();

            if ($list) {
                $order_ids = array_column($list, 'id');
                $goods = LihuaOrderGoods::ordersGoodsList($order_ids);
                foreach ($list as $k => $v) {
                    $list[$k]['goods'] = isset($goods[$v['id']]) ? json_encode($goods[$v['id']]) : [];
                }
            }

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
}
