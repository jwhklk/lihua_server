<?php

namespace app\admin\controller\goods;

use app\common\controller\Backend;

/**
 * 商品分类
 *
 * @icon fa fa-circle-o
 */
class LihuaCategory extends Backend
{
    protected $cityLimit = true;
    protected $noNeedRight = ['sel'];

    /**
     * LihuaCategory模型对象
     * @var \app\common\model\goods\LihuaCategory
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\goods\LihuaCategory;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $total = $this->model
                ->with(['lihuacity'])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['lihuacity'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {

                $row->getRelation('lihuacity')->visible(['name']);
            }
            $list = collection($list)->toArray();

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 选择分类
     *
     * @internal
     */
    public function sel()
    {
        $city_id = (int)$this->request->get('city_id');
        if (!$city_id) $city_id = $this->auth->city_id;

        $list = $this->model::getCityCategoryList($city_id);
        return json($list);
    }

}
