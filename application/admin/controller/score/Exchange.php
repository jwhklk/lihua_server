<?php

namespace app\admin\controller\score;

use app\common\controller\Backend;
use app\common\model\general\LihuaCity;
use app\common\model\LihuaExchange;

/**
 * 积分兑换
 *
 * @icon fa fa-circle-o
 */
class Exchange extends Backend
{
    protected $cityLimit = true;

    /**
     * LihuaExchange模型对象
     * @var LihuaExchange
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new LihuaExchange();
    }

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $map['sg_type'] = 1;//仅显示普通商品
            $total = $this->model
                    ->where($where)
                    ->where($map)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->where($map)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            $cityArr = LihuaCity::getCityArr();
            $list    = collection($list)->toArray();
            foreach ($list as $k => $v) {
                $list[$k]['city_name'] = isset($cityArr[$v['city_id']]) ? $cityArr[$v['city_id']] : '';
            }

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 处理
     */
    public function done($id = 0)
    {
        if (!is_numeric($id)) $this->error('参数错误');
        if ($this->request->isAjax()) {
            $row = $this->request->post('row/a');
            $row['dostat'] = 1;
            $row['dotime'] = time();
            $this->model->where('id', $id)->update($row);
            $this->success();
        }

        $map['id'] = $id;
        if ($this->auth->city_id > 0) $map['city_id'] = $this->auth->city_id;

        $row = $this->model->where($map)->find();
        $this->view->assign('row', $row);
        return $this->view->fetch();
    }
}
