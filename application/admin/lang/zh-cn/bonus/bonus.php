<?php

return [
    'Id'       => 'ID',
    'City_id'  => '城市ID',
    'Name'     => '红包名称',
    'Type'     => '红包类型',
    'Type 1'   => '新人红包',
    'Type 2'   => '满减红包',
    'Type 3'   => '无门槛红包',
    'Reach'    => '满',
    'Reduce'   => '减',
    'Status'   => '状态',
    'Status 0' => '禁用',
    'Status 1' => '启用',
    'Status 2' => '删除'
];
