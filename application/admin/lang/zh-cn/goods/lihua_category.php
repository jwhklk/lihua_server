<?php

return [
    'Id'          => 'ID',
    'City_id'     => '城市ID',
    'Name'        => '分类名称',
    'Weigh'       => '排序',
    'Status'      => '状态',
    'Status 0'    => '禁用',
    'Status 1'    => '启用',
    'Delete_time' => '删除时间'
];
