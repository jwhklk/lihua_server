<?php

return [
    'Id'       => 'ID',
    'City_id'  => '城市ID',
    'Name'     => '商品名称',
    'Type'     => '商品类型',
    'Type 1'   => '普通商品',
    'Type 2'   => '红包卡券',
    'Des'      => '商品描述',
    'Pic'      => '商品图片',
    'Score'    => '所需积分',
    'Status'   => '状态',
    'Status 0' => '禁用',
    'Status 1' => '启用',
    'Status 2' => '删除',
    'Bonus_id' => '红包ID'
];
