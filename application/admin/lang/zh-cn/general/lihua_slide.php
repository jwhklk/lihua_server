<?php

return [
    'Id'             => 'ID',
    'City_id'        => '城市ID',
    'Title'          => '标题',
    'Pic'            => '图片',
    'Content'        => '内容',
    'Url'            => '跳转链接',
    'Start_date'     => '开始日期',
    'End_date'       => '结束日期',
    'Create_time'    => '创建时间',
    'Weigh'          => '排序',
    'Lihuacity.name' => '城市名称'
];
