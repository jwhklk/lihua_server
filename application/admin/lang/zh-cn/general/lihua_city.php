<?php

return [
    'Id'          => 'ID',
    'Name'        => '城市名称',
    'Tel'         => '订餐电话',
    'Am_start'    => '上午开始时间',
    'Am_end'      => '上午结束时间',
    'Pm_start'    => '下午开始时间',
    'Pm_end'      => '下午结束时间',
    'Min_price'   => '起送价',
    'Deliver_fee' => '送餐费',
    'Weigh'       => '排序',
    'Status'      => '状态',
    'Status 0'    => '禁用',
    'Status 1'    => '启用'
];
