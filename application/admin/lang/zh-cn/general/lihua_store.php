<?php

return [
    'Id'          => 'ID',
    'City_id'     => '城市ID',
    'Name'        => '门店名称',
    'Tel'         => '门店电话',
    'Addr'        => '门店地址',
    'Lng'         => '经度',
    'Lat'         => '纬度',
    'Area'        => '配送区域',
    'Manager'     => '门店经理',
    'Manager_tel' => '经理手机',
    'Status'      => '状态',
    'Status 0'    => '禁用',
    'Status 1'    => '启用',
    'Delete_time' => '删除时间'
];
