<?php

namespace app\admin\command;

use app\common\model\general\LihuaCity;
use app\common\model\goods\LihuaGoods;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use fast\Http;
use think\Db;

class CopyOldGoodsData extends Command
{

    protected function configure()
    {
        $this->setName('copyOldGoodsData')->setDescription('复制老系统商品数据');
    }

    protected function execute(Input $input, Output $output)
    {
        $cityArr = LihuaCity::getCityArr();
        foreach ($cityArr as $k => $v) {
            $this->copyData($k, $v);
        }
        $output->info(date('Y-m-d H:i:s').' 复制老系统商品数据成功');
    }

    protected function copyData($city_id, $city_name)
    {
        $res = Http::post('https://api.lihua.com/index/Good/copyOldGoodData',
            ['cityname' => $city_name, 'current_date' => date('Y-m-d')]);
        if ($res) {
            $arr = json_decode($res, true);
            if (!is_null($arr)) {
                if ($arr['status'] == 1) {
                    $cateList = $arr['data']['cate_list'];//分类列表
                    $goodList = $arr['data']['good_list'];//商品列表
                    $cateCnt  = count($cateList);
                    foreach ($cateList as $k => $v) {
                        $data = [
                            'old_id'  => $v['id'],
                            'city_id' => $city_id,
                            'name'    => $v['title'],
                            'weigh'   => $cateCnt,
                            'status'  => 1,
                        ];
                        $cateCnt--;
                        try {
                            $row = Db::name('lihua_category')->where('old_id', $v['id'])->find();
                            if ($row) {
                                Db::name('lihua_category')->where('id', $row['id'])->update($data);
                            } else {
                                Db::name('lihua_category')->insert($data);
                            }
                        } catch (\Exception $e) {
                            $this->log($e->getMessage());
                        }
                    }

                    $cates = Db::name('lihua_category')->where('city_id', $city_id)->column('old_id,id');
                    $goodList = array_reverse($goodList);
                    foreach ($goodList as $k => $v) {
                        $data = [
                            'old_id'        => $v['id'],
                            'city_id'       => $city_id,
                            'category_id'   => isset($cates[$v['category_id']]) ? $cates[$v['category_id']] : 0,
                            'name'          => $v['name'],
                            'short_name'    => $v['short'],
                            'description'   => $v['desc'],
                            'pic'           => $v['pic'],
                            'type'          => $v['is_alone_sale'] == 1 ? 1 : 2,
                            'price'         => $v['price'],
                            'box_fee'       => $v['box_fee'],
                            'is_alone_sale' => $v['is_alone_sale'] == 2 ? 0 : 1,
                            'sale_start'    => $v['sale_start_date'],
                            'sale_end'      => $v['sale_end_date'],
                            'limit_num'     => $v['limit_num'],
                            'at_least'      => $v['at_least'],
                            'status'        => 1,
                        ];
                        try {
                            $row = LihuaGoods::where('old_id', $v['id'])->field('id')->find();
                            if ($row) {
                                LihuaGoods::where('id', $row['id'])->update($data);
                            } else {
                                LihuaGoods::create($data);
                            }
                        } catch (\Exception $e) {
                            $this->log($e->getMessage());
                        }
                    }
                } else {
                    $this->log($arr['message']);
                }
            } else {
                $this->log('源数据JSON: ' . json_last_error_msg());
            }
        } else {
            $this->log('未获取到' . $city_name . '今日商品数据');
        }
    }

    protected function log($msg = '')
    {
        echo date('Y-m-d H:i:s ') . $msg . "\n";
    }

}
