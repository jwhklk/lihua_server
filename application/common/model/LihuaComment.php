<?php

namespace app\common\model;

use think\Model;

/**
 * 评价管理
 */
class LihuaComment extends Model
{

    // 表名
    protected $table = 'lihua_comment';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [];

}
