<?php

namespace app\common\model;

use think\Model;

/**
 * 分享领红包
 */
class LihuaHongbao extends Model
{

    // 表名
    protected $table = 'lihua_hongbao';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'create_time';
    protected $updateTime = false;

    // 追加属性
    protected $append = [];

}
