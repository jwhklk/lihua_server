<?php

namespace app\common\model;

use think\Model;

/**
 * 订单支付记录
 */
class LihuaOrderPayment extends Model
{

    // 表名
    protected $table = 'lihua_order_payment';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = false;

    // 追加属性
    protected $append = [];

}
