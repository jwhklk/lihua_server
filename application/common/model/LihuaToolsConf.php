<?php

namespace app\common\model;

use think\Model;

/**
 * 营销工具配置模型
 */
class LihuaToolsConf extends Model
{

    // 表名
    protected $table = 'lihua_tools_conf';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [];

}
