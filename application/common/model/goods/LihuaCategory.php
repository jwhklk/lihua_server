<?php

namespace app\common\model\goods;

use think\Model;
use traits\model\SoftDelete;


class LihuaCategory extends Model
{

    // 表名
    protected $table = 'lihua_category';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 启用软删除
    use SoftDelete;

    // 追加属性
    protected $append = [];
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    // 获取城市分类数组
    public static function getCityCategoryArr($city_id = 0)
    {
        return self::where(['city_id' => $city_id, 'status' => 1])->order('weigh','desc')->column('id,name');
    }

    // 获取城市分类ID
    public static function getCityCategoryIds($city_id = 0)
    {
        return self::where(['city_id' => $city_id, 'status' => 1])->order('weigh', 'desc')->column('id');
    }

    // 获取城市分类
    public static function getCityCategoryList($city_id = 0)
    {
        $list = self::all(function ($query) use ($city_id) {
            $query->where(['city_id' => $city_id, 'status' => 1])->field('id,name')->order('weigh', 'desc');
        });
        return $list;
    }

    
    public function getStatusList()
    {
        return ['0' => '禁用', '1' => '启用'];
    }

    // 关联城市模型
    public function lihuacity()
    {
        return $this->belongsTo('app\common\model\general\LihuaCity', 'city_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
