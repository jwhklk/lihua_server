<?php

namespace app\common\model\goods;

use think\Model;


class LihuaStock extends Model
{

    // 表名
    protected $table = 'lihua_stock';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [];


}
