<?php

namespace app\common\model;

use think\Model;

/**
 * 微信用户
 */
class LihuaWxuser extends Model
{

    // 表名
    protected $table = 'lihua_wxuser';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 追加属性
    protected $append = [];

}
