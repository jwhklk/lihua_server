<?php

namespace app\common\model\order;

use think\Model;


class Order extends Model
{

    // 表名
    protected $table = 'lihua_order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        //status_text
    ];

    public function getStatusList()
    {
        return ["0" => "已取消", "1" => "待支付", "2" => "待确认", "3" => "待分配", "4" => "待派送", "5" => "派送中", "6" => "已完成"];
    }

    /*public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }*/


}
