<?php

namespace app\common\model;

use think\Model;

/**
 * 订单
 */
class LihuaOrder extends Model
{

    // 表名
    protected $table = 'lihua_order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 追加属性
    protected $append = [];

    // 订单状态
    public static function getStatusList()
    {
        return ['订单已取消', '等待支付', '等待商家接单', '等待骑手配送', '等待骑手配送', '订单派送中', '订单已送达'];
    }

    // 订单进度
    public static function getOrderProcess($order_sn, $user_id)
    {
        $list = [];
        $map['sn'] = $order_sn;
        if ($user_id > 0) {
            $map['user_id'] = $user_id;
        }
        $res = self::get(function ($query) use ($map) {
            $query->where($map)->field('id,pay_type,add_time,pay_time,confirm_time,rider_time,finish_time,cancel_time,status');
        });

        if ($res) {
            $row = $res->toArray();
            $add_time     = $row['add_time']     ? date('m-d H:i', strtotime($row['add_time']))     : '';
            $cancel_time  = $row['cancel_time']  ? date('m-d H:i', strtotime($row['cancel_time']))  : '';
            $pay_time     = $row['pay_time']     ? date('m-d H:i', strtotime($row['pay_time']))     : '';
            $confirm_time = $row['confirm_time'] ? date('m-d H:i', strtotime($row['confirm_time'])) : '';
            $rider_time   = $row['rider_time']   ? date('m-d H:i', strtotime($row['rider_time']))   : '';
            $finish_time  = $row['finish_time']  ? date('m-d H:i', strtotime($row['finish_time']))  : '';

            $list = [
                ['status' => '订单提交成功', 'time' => $add_time, 'tips' => '', 'pass' => 1, 'ing' => 0],
                ['status' => '等待支付', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 0],
                ['status' => '商家接单', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 0],
                ['status' => '骑手配送', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 0],
                ['status' => '等待送达', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 0],
            ];

            $list_1 = ['status' => '订单已支付', 'time' => $pay_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
            if ($row['pay_type'] == 4) {
                $list_1 = ['status' => '已选择货到付款', 'time' => $add_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
            }
            switch ($row['status']) {
                case 0:
                    $arr  = ['status' => '订单已取消', 'time' => $cancel_time, 'tips' => '你取消了订单', 'pass' => 1, 'ing' => 0];
                    $list = [$list[0], $arr];
                    break;

                case 1:
                    $list[1] = ['status' => '等待支付', 'time' => '', 'tips' => '逾期未支付，订单将自动取消', 'pass' => 0, 'ing' => 1];
                    break;

                case 2:
                    $list[1] = $list_1;
                    $list[2] = ['status' => '等待商家接单', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 1];
                    break;

                case 3:
                case 4:
                    $list[1] = $list_1;
                    $list[2] = ['status' => '商家已接单', 'time' => $confirm_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
                    $list[3] = ['status' => '等待骑手配送', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 1];
                    break;

                case 5:
                    $list[1] = $list_1;
                    $list[2] = ['status' => '商家已接单', 'time' => $confirm_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
                    $list[3] = ['status' => '骑手已接单', 'time' => $rider_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
                    $list[5] = $list[4];
                    $list[4] = ['status' => '订单派送中', 'time' => '', 'tips' => '', 'pass' => 0, 'ing' => 1];
                    break;

                case 6:
                    $list[1] = $list_1;
                    $list[2] = ['status' => '商家已接单', 'time' => $confirm_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
                    $list[3] = ['status' => '骑手已接单', 'time' => $rider_time, 'tips' => '', 'pass' => 1, 'ing' => 0];
                    $list[4] = ['status' => '订单已送达', 'time' => $finish_time, 'tips' => '感谢你对丽华快餐的信任，期待再次光临', 'pass' => 1, 'ing' => 0];
                    break;

                default:

                    break;
            }
        }
        return $list;
    }

}
