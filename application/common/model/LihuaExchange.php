<?php

namespace app\common\model;

use think\Model;

/**
 * 积分兑换
 */
class LihuaExchange extends Model
{

    // 表名
    protected $table = 'lihua_exchange';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    protected $updateTime = false;

    // 追加属性
    protected $append = [];

}
