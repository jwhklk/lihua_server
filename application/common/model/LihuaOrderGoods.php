<?php

namespace app\common\model;

use think\Model;

/**
 * 订单商品
 */
class LihuaOrderGoods extends Model
{

    // 表名
    protected $table = 'lihua_order_goods';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [];


    //多订单商品列表，订单id为索引
    public static function ordersGoodsList($order_ids = [])
    {
        $goods = [];
        if (is_array($order_ids)) {
            $goodsList = self::whereIn('order_id', $order_ids)->select();
            foreach ($goodsList as $k => $v) {
                $goods[$v['order_id']][] = [
                    'id'          => $v['goods_id'],
                    'name'        => $v['name'],
                    'short_name'  => $v['short_name'],
                    'price'       => round($v['price'], 2),
                    'pic'         => $v['pic'],
                    'num'         => $v['num'],
                    'num_price'   => round($v['num_price'], 2),
                    'box_fee'     => round($v['box_fee'], 2),
                ];
            }
        }
        return $goods;
    }
}
