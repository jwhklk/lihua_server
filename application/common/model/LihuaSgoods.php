<?php

namespace app\common\model;

use think\Model;


class LihuaSgoods extends Model
{
    // 表名
    protected $table = 'lihua_sgoods';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [];
    

    public function getTypeList()
    {
        return ['1' => '普通商品', '2' => '红包卡券'];
    }

    public function getStatusList()
    {
        return ['0' => '禁用', '1' => '启用'];
    }

}
