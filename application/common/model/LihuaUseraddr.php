<?php

namespace app\common\model;

use think\Model;

/**
 * 用户收货地址
 */
class LihuaUseraddr extends Model
{

    // 表名
    protected $table = 'lihua_useraddr';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [];

}
