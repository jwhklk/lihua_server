<?php

namespace app\common\model;

use think\Model;

/**
 * 用户红包
 */
class LihuaUserbonus extends Model
{

    // 表名
    protected $table = 'lihua_userbonus';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 追加属性
    protected $append = [];

    // 领取红包 $param 数组字段
    // city_id、user_id、bonus_id、name、type、reach、reduce、start_date、end_date
    public static function addUserBonus($param = [])
    {
        if ($param && isset($param['user_id'])) {
            $days = 2;//有效天数
            $num  = 1;//红包个数
            if (isset($param['num'])) {
                $num = (int)$param['num']; unset($param['num']);
            }
            if (isset($param['days'])) {
                $days = (int)$param['days']; unset($param['days']);
            }
            try {
                if (!isset($param['type'])) {
                    $param['type'] = $param['reach'] > 0 ? 2 : 3;// 2满减 3无门槛
                }
                if (!isset($param['name'])) {
                    $param['name'] = $param['reduce'] . '元红包';
                }
                $param['start_date'] = date('Y-m-d');
                $param['end_date'] = $days > 1 ? date('Y-m-d', strtotime("+{$days} day")) : $param['start_date'];
                for ($i = 0; $i < $num; $i++) {
                    self::create($param);
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        return true;
    }

    // 更新失效红包
    public static function updateExpireBonusStatus($user_id)
    {
        $day = date('Y-m-d');
        self::update(['status' => 2], ['user_id' => $user_id, 'status' => 0, 'end_date' => ['<', $day]]);
        return true;
    }

    // 关联用户模型
    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
