<?php

namespace app\common\model;

use think\Model;

/**
 * 用户发票信息
 */
class LihuaUserinvo extends Model
{

    // 表名
    protected $table = 'lihua_userinvo';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 追加属性
    protected $append = [
        'type_text'
    ];

    public function getTypeList()
    {
        return ['1' => '个人', '2' => '单位'];
    }

    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list  = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

}
