define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'order/order/index' + location.search,
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: 'lihua_order',
                }
            });

            var table = $("#table");

            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                columns: [
                    [
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'store_name', title: '城市门店', operate: false, formatter: Controller.api.formatter.store},
                        {field: 'sn', title: '订单号'},
                        {field: 'goods', title: '订单商品', align: 'left', operate: false, formatter: Controller.api.formatter.goods},
                        {field: 'goods_amount', title: '商品总价', operate: false},
                        {field: 'order_amount', title: '订单总价', operate: false},
                        {field: 'pay_type', title: '支付方式', operate: false, formatter: Controller.api.formatter.paytype},
                        {field: 'pay_status', title: '支付状态', operate: false, formatter: Controller.api.formatter.paystatus},
                        {field: 'invo_title', title: '开票信息', align: 'left', operate: false, formatter: Controller.api.formatter.invo},
                        {field: 'consignee', title: '下单人', align: 'left', operate: false, formatter: Controller.api.formatter.consignee},
                        {field: 'mobile', title: '联系手机', visible: false},
                        {field: 'add_time', title: '下单时间', operate: false, formatter:Controller.api.formatter.mytime},
                        {field: 'deliver_time', title: '送餐时间', operate: false, formatter:Controller.api.formatter.mytime},
                        {field: 'status', title: '订单状态', searchList: {"0":"已取消","1":"待支付","2":"待确认","3":"待分配","4":"待派送","5":"派送中","6":"已完成"}, custom:{"0":"danger","1":"default","2":"warning","3":"purple","4":"maroon","5":"info","6":"success"}, formatter: Table.api.formatter.label},
                        //{field: 'operate', title: '操作', table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                store: function (value, row, index) {
                    return row.city_name + value;
                },
                consignee: function (value, row, index) {
                    return value + ' <span class="text-warning" style="font-size:16px;">' + row.mobile +
                        '</span><br><span style="color:#999;">' + row.address + '</span>';
                },
                goods: function (value, row, index) {
                    var ret = '';
                    if (row.goods) {
                        var obj = JSON.parse(row.goods);
                        $.each(obj, function (k, v) {
                            ret += '<span class="text-danger">' + v.num + ' ×</span> '+ v.name +'<br>';
                        });
                    }
                    ret += '<span style="color:#999;">餐具：</span>';
                    if (row.tableware == 1) {
                        ret += '<span class="text-orange">需要</span>';
                    } else {
                        ret += '<span style="color:#666;">不需要</span>';

                    }
                    if (row.remark) {
                        ret += '<br><span style="color:#999;">备注：</span><span style="color:#666;">' + row.remark + '</span>';
                    }
                    return ret;
                },
                mytime: function (value, row, index) {
                    var arr = value.split(' ');
                    return arr[0] + '<br>' + arr[1];
                },
                paytype: function (value, row, index) {
                    var ret = '';
                    switch (value) {
                        case 1:
                            ret = '<span class="label label-info">支付宝支付</span>';
                            break;
                        case 2:
                            ret = '<span class="label label-success">微信支付</span>';
                            break;
                        case 3:
                            ret = '<span class="label label-danger">丽华钱包</span>';
                            break;
                        case 4:
                            ret = '<span class="label label-warning">货到付款</span>';
                            break;
                    }
                    return ret;
                },
                paystatus: function (value, row, index) {
                    if (value === 1) {
                        return '<span class="label label-success">已支付</span>';
                    } else {
                        return '<span class="label label-default">未支付</span>';
                    }
                },
                invo: function (value, row, index) {
                    var str = '';
                    if (value) str += '抬头：' + value;
                    if (row.invo_taxno) str += '<br>税号：' + row.invo_taxno;
                    return str;
                }
            }
        }
    };
    return Controller;
});