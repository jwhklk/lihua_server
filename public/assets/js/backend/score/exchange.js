define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'score/exchange/index' + location.search,
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: 'lihua_exchange',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: true,
                searchFormVisible: true,
                commonSearch: true,
                exportTypes: ['excel'],
                exportOptions: {
                    fileName:Moment().format("YYYYMMDD-HHmm") + '-积分兑换',
                    ignoreColumn: [-1, -2],
                    mso: {
                        //fileFormat:'xlsx',
                        onMsoNumberFormat: function (cell, row, col) {
                            return !isNaN($(cell).text())?'\\@':'';
                        },
                    }
                },
                pageList: [10, 25, 50, 100],
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'),
                            formatter: function (value, row, index) {return row.city_name;}},
                        {field: 'sg_name', title: '兑换商品', operate: 'LIKE'},
                        {field: 'num', title: '兑换数量', operate: false},
                        {field: 'score', title: '消耗积分', operate: false},
                        {field: 'consignee', title: '兑换人'},
                        {field: 'mobile', title: '手机号'},
                        {field: 'address', title: '收货地址', operate: false, align: 'left', width: 300},
                        {field: 'create_time', title: '兑换时间', width: 100, operate: false, formatter: Table.api.formatter.datetime},
                        {field: 'dostat', title: '状态', visible: false, searchList: {"0":"待处理","1":"已处理"}, formatter: Table.api.formatter.datetime},
                        {field: 'dotime', title: '处理时间', width: 100, operate: false, formatter: Controller.api.formatter.dotime},
                        {field: 'domark', title: '处理备注', operate: false},
                        {field: 'operate', title: '操作', table: table, events: Table.api.events.operate, formatter: Controller.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            // 当内容渲染完成后
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".maxBox").data("area", ["70%", "80%"]);
                $(".minBox").data("area", ["600px", "450px"]);
            });
        },
        done: function () {
            Controller.api.bindevent();
            $("#submit").on("click", function(){
                var that = this;
                parent.Layer.confirm('确定该兑换已处理完毕吗？', {
                    icon: 3, title: "操作提醒", shade: 0.3, closeBtn: 0, zIndex: parent.Layer.zIndex + 1
                }, function(){
                    $(that).closest("form").trigger("submit");
                    parent.Layer.closeAll();
                });
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                dotime: function (value, row, index) {
                    if (value > 0) {
                        return Table.api.formatter.datetime.call(this, value, row, index);
                    } else {
                        return '';
                    }
                },
                operate: function (value, row, index) {
                    if (row.dostat === 0) {
                        return '<a data-toggle="tooltip" title="处理兑换" data-shade="0.3" href="score/exchange/done/id/'+ row.id +'" class="btn btn-xs btn-info btn-dialog">处理</a>';
                    } else {
                        return '';
                    }
                },
            }
        }
    };
    return Controller;
});