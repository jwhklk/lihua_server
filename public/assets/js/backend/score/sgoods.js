define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'score/sgoods/index' + location.search,
                    add_url: 'score/sgoods/add',
                    edit_url: 'score/sgoods/edit',
                    del_url: 'score/sgoods/del',
                    multi_url: '',
                    table: 'lihua_sgoods',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'),
                            formatter: function (value, row, index) {return row.city_name;}},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        //{field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2')}, custom:{"1":"info", "2":"warning"}, formatter: Table.api.formatter.label},
                        {field: 'pic', title: __('Pic'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'score', title: __('Score'), operate: false},
                        {field: 'limit_num', title: '限购数量', operate: false, formatter: Controller.api.formatter.limit_num},
                        {field: 'status', title: '状态', searchList: {"0":"禁用","1":"启用"}, formatter: Controller.api.formatter.status},
                        //{field: 'bonus_id', title: __('Bonus_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            var bonusDiv = '<div id="bonusDiv" class="form-group">\n' +
                '        <label class="control-label col-xs-12 col-sm-2">红包卡券:</label>\n' +
                '        <div class="col-xs-12 col-sm-8">\n' +
                '            <input id="c-bonus_id" data-rule="required" data-source="bonus/bonus/index" data-order-by="id desc" class="form-control selectpage" name="row[bonus_id]" type="text" value="">\n' +
                '        </div>\n' +
                '    </div>';
            $(".radio").on("change", "input[name='row[type]']", function () {
                if ($(this).val() == 2) {
                    $("#typeDiv").after(bonusDiv);
                    Form.events.selectpage($("form"));
                } else {
                    $("#bonusDiv").remove();
                }
            });
        },
        edit: function () {
            Controller.api.bindevent();
            var bonusDiv = '<div id="bonusDiv" class="form-group">\n' +
                '        <label class="control-label col-xs-12 col-sm-2">红包卡券:</label>\n' +
                '        <div class="col-xs-12 col-sm-8">\n' +
                '            <input id="c-bonus_id" data-rule="required" data-source="bonus/bonus/index" data-order-by="id desc" class="form-control selectpage" name="row[bonus_id]" type="text" value="">\n' +
                '        </div>\n' +
                '    </div>';
            if (typeof $("#bonusDiv").html() !== 'undefined') {
                var html = $("#bonusDiv").html();
                bonusDiv = '<div id="bonusDiv" class="form-group">\n' + html + '\n</div>';
            }

            $(".radio").on("change", "input[name='row[type]']", function () {
                if ($(this).val() == 2) {
                    $("#hiddenBonusId").remove();
                    $("#typeDiv").after(bonusDiv);
                    Form.events.selectpage($("form"));
                } else {
                    $("#bonusDiv").remove();
                    $("#typeDiv").after('<input id="hiddenBonusId" type="hidden" name="row[bonus_id]" value="0">');
                }
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                limit_num: function (value, row, index) {
                    return value == 0 ? '无限制' : value;
                },
                status: function (value, row, index) {
                    var title = '点击禁用';
                    var cls = 'fa-toggle-on text-success';
                    if (value === 0) {
                        title = '点击启用';
                        cls = 'fa-toggle-on fa-rotate-180 text-gray';
                    }
                    return '<a style="cursor:pointer" data-url="score/sgoods/status/ids/' + row.id + '" data-table-id="table" data-refresh="1" class="btn-ajax" data-toggle="tooltip" title="' + title + '"><i class="fa ' + cls + ' fa-2x"></i></a>';
                }
            }
        }
    };
    return Controller;
});