define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                }
            });

            var table = $("#table");

            //在表格内容渲染完成后回调的事件
            table.on('post-body.bs.table', function (e, json) {
                $("tbody tr[data-index]", this).each(function () {
                    if (parseInt($("td:eq(1)", this).text()) == Config.admin.id) {
                        $("input[type=checkbox]", this).prop("disabled", true);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: false,
                commonSearch: false,
                columns: [
                    [
                        //{field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'username', title: __('Username')},
                        {field: 'nickname', title: __('Nickname')},
                        {field: 'groups_text', title: __('Group'), operate:false, formatter: Table.api.formatter.label},
                        {field: 'city_text', title: '管理城市', operate:false},
                        //{field: 'email', title: __('Email')},
                       //{field: 'status', title: __("Status"), formatter: Table.api.formatter.status},
                        {field: 'status', title: '状态', operate: false, formatter: Controller.api.formatter.status},
                        {field: 'logintime', title: __('Login time'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.id == Config.admin.id){
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            formatter: {
                status: function (value, row, index) {
                    if(row.id == Config.admin.id){
                        return '';
                    } else {
                        var title  = '点击禁用';
                        var cls    = 'fa-toggle-on text-success';
                        if (value === 'hidden') {
                            title  = '点击启用';
                            cls    = 'fa-toggle-on fa-rotate-180 text-gray';
                        }
                        return '<a style="cursor:pointer" data-url="auth/admin/status/ids/'+row.id+'" data-table-id="table" data-refresh="1" class="btn-ajax" data-toggle="tooltip" title="'+title+'"><i class="fa ' + cls + ' fa-2x"></i></a>';
                    }
                }
            }
        }
    };
    return Controller;
});