define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'general/lihua_city/index' + location.search,
                    add_url: 'general/lihua_city/add',
                    edit_url: 'general/lihua_city/edit',
                    del_url: 'general/lihua_city/del',
                    multi_url: '',
                    table: 'lihua_city',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                search: false,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: false,
                commonSearch: false,
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'tel', title: __('Tel')},
                        {field: 'am_start', title: '营业时段', formatter: function (value, row, index) {
                            return value + ' ~ ' + row.am_end + ' , ' + row.pm_start + ' ~ ' + row.pm_end;
                        }},
                        {field: 'min_price', title: __('Min_price'), operate: false},
                        {field: 'deliver_fee', title: __('Deliver_fee'), operate:false},
                        {field: 'id', title: 'APP支付设置', operate:false, formatter: Controller.api.formatter.appPay},
                        {field: 'weigh', title: __('Weigh')},
                        {field: 'status', title: '状态', searchList: {"0":"禁用","1":"启用"}, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            // 当内容渲染完成后
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dc").data("area", ["80%", "80%"]);
                $(".btn-xc").data("area", ["600px", "450px"]);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status: function (value, row, index) {
                    var title  = '点击禁用';
                    var cls    = 'fa-toggle-on text-success';
                    if (value === 0) {
                        title  = '点击启用';
                        cls    = 'fa-toggle-on fa-rotate-180 text-gray';
                    }
                    return '<a style="cursor:pointer" data-url="general/lihua_city/status/ids/'+row.id+'" data-table-id="table" data-refresh="1" class="btn-ajax" data-toggle="tooltip" title="'+title+'"><i class="fa ' + cls + ' fa-2x"></i></a>';
                },
                appPay: function (value, row, index) {
                    var alipay = '<a data-shade="0.3" data-toggle="tooltip" data-title="支付宝支付设置" href="general/payset/alipay/city_id/'+ row.id +'" class="btn btn-xs btn-info btn-dialog" >支付宝</a>';
                    var wxpay  = '<a data-shade="0.3" data-toggle="tooltip" data-title="微信支付设置" href="general/payset/wxpay/city_id/'+ row.id +'" class="btn btn-xs btn-success btn-dialog" >微信</a>';
                    return alipay + ' ' + wxpay;
                }
            }
        }
    };
    return Controller;
});