define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'general/lihua_store/index' + location.search,
                    add_url: 'general/lihua_store/add',
                    edit_url: 'general/lihua_store/edit',
                    del_url: 'general/lihua_store/del',
                    multi_url: '',
                    table: 'lihua_store',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                columns: [
                    [
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'), formatter:
                                function (value, row, index) { return row.lihuacity.name;}},
                        {field: 'name', title: '门店'},
                        {field: 'tel', title: '门店电话', operate: false},
                        {field: 'addr', title: '门店地址', align: 'left', operate: false},
                        //{field: 'lng', title: '经纬度', operate: false, formatter: Controller.api.formatter.lnglat},
                        {field: 'manager', title: '门店经理', operate: false, formatter: Controller.api.formatter.manager},
                        {field: 'status', title: '状态', searchList: {"0":"禁用","1":"启用"}, url: 'general/lihua_store/status', formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                onLoadSuccess: function () {
                    $(".btn-editone").data("area", ["100%", "100%"]);
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            $("#selPoint").click(function () {
                var city_id = $("input[name='row[city_id]']").val();
                if (city_id) {
                    Fast.api.open('general/lihua_store/selpoint?city_id='+ city_id, '选取门店坐标', {area:['75%','90%'], shade:0.3});
                } else {
                    Toastr.error('请先选择城市');
                }
            });
            $("#selArea").click(function () {
                var id = $(this).data('id');
                var lng = $("#c-lng").val();
                var lat = $("#c-lat").val();
                if (lng && lat) {
                    Fast.api.open('general/lihua_store/selarea?store_id='+id+'&lng='+lng+'&lat='+lat, '选取配送范围（请在地图中标三个点后再拖动各点调整范围）', {area:['80%','95%'], shade:0.3});
                } else {
                    Toastr.error('请先选取门店坐标');
                }
            });
        },
        edit: function () {
            Controller.api.bindevent();
            $("#selPoint").click(function () {
                var lng = $("#c-lng").val();
                var lat = $("#c-lat").val();
                Fast.api.open('general/lihua_store/selpoint?lng='+lng+'&lat='+lat, '选取门店坐标', {area:['75%','90%'], shade:0.3});
            });
            $("#selArea").click(function () {
                var id = $(this).data('id');
                Fast.api.open('general/lihua_store/selarea?store_id='+id, '选取配送范围（请在地图中标三个点后再拖动各点调整范围）', {area:['80%','95%'], shade:0.3});
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                lnglat: function (value, row, index) {
                    if (value && row.lat) {
                        return value + ',' + row.lat;
                    } else {
                        return '';
                    }
                },
                manager: function (value, row, index) {
                    return value + ' <span class="text-warning">' + row.manager_tel + '</span>';
                }
            }
        }
    };
    return Controller;
});