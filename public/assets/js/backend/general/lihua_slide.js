define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'selectpage'], function ($, undefined, Backend, Table, Form, selectPage) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'general/lihua_slide/index' + location.search,
                    add_url: 'general/lihua_slide/add',
                    edit_url: 'general/lihua_slide/edit',
                    del_url: 'general/lihua_slide/del',
                    multi_url: '',
                    table: 'lihua_slide',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                search: false,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: false,
                commonSearch: false,
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'city_id', title: __('City_id'), visible: false},
                        {field: 'lihuacity.name', title: '城市'},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'pic', title: __('Pic'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'start_date', title: __('Start_date'), operate:'RANGE', addclass:'datetime'},
                        {field: 'end_date', title: __('End_date'), operate:'RANGE', addclass:'datetime'},
                        {field: 'weigh', title: __('Weigh'), operate: false},
                        //{field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});