define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'goods/lihua_category/index' + location.search,
                    add_url: 'goods/lihua_category/add',
                    edit_url: 'goods/lihua_category/edit',
                    del_url: 'goods/lihua_category/del',
                    multi_url: '',
                    table: 'lihua_category',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                columns: [
                    [
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'), formatter:
                                function (value, row, index) { return row.lihuacity.name;}},
                        {field: 'name', title: '分类名称', operate: 'LIKE'},
                        {field: 'weigh', title: '排序', operate: false},
                        {field: 'status', title: '状态', searchList: {"0":"禁用","1":"启用"}, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status: function (value, row, index) {
                    var title  = '点击禁用';
                    var cls    = 'fa-toggle-on text-success';
                    if (value === 0) {
                        title  = '点击启用';
                        cls    = 'fa-toggle-on fa-rotate-180 text-gray';
                    }
                    return '<a style="cursor:pointer" data-url="goods/lihua_category/status/ids/'+row.id+'" data-table-id="table" data-refresh="1" class="btn-ajax" data-toggle="tooltip" title="'+title+'"><i class="fa ' + cls + ' fa-2x"></i></a>';
                },
            }
        }
    };
    return Controller;
});