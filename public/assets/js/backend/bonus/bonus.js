define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bonus/bonus/index' + location.search,
                    add_url: 'bonus/bonus/add',
                    edit_url: 'bonus/bonus/edit',
                    del_url: 'bonus/bonus/del',
                    multi_url: '',
                    table: 'lihua_bonus',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                columns: [
                    [
                        //{checkbox: true},
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'),
                            formatter: function (value, row, index) {return row.city_name;}},
                        {field: 'type', title: __('Type'), searchList: {"2":__('Type 2'),"3":__('Type 3')}, custom:{"2":"info", "3":"success"}, formatter: Table.api.formatter.label},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'reach', title: __('Reach'), operate: false},
                        {field: 'reduce', title: __('Reduce'), operate: false},
                        {field: 'days', title: '有效天数', operate: false},
                        {field: 'status', title: '状态', operate: false, searchList: {"0":"禁用","1":"启用"}, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            Controller.api.wumenkan();

        },
        edit: function () {
            Controller.api.bindevent();
            Controller.api.wumenkan();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            wumenkan: function () {
                $("input[name='row[type]']").click(function () {
                    if ($("input[name='row[type]']:checked").val() == 3) {
                        $("#c-reach").val('0').prop('readonly', true);
                    } else {
                        $("#c-reach").prop('readonly', false);
                    }
                });
            },
            formatter: {
                status: function (value, row, index) {
                    var title = '点击禁用';
                    var cls = 'fa-toggle-on text-success';
                    if (value === 0) {
                        title = '点击启用';
                        cls = 'fa-toggle-on fa-rotate-180 text-gray';
                    }
                    return '<a style="cursor:pointer" data-url="bonus/bonus/status/ids/' + row.id + '" data-table-id="table" data-refresh="1" class="btn-ajax" data-toggle="tooltip" title="' + title + '"><i class="fa ' + cls + ' fa-2x"></i></a>';
                }
            }
        }
    };
    return Controller;
});