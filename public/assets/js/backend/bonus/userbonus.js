define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bonus/userbonus/index' + location.search,
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: 'lihua_userbonus',
                }
            });

            var table = $("#table");
            $.fn.bootstrapTable.locales[Table.defaults.locale]["formatSearch"] = function(){return "搜索ID";};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search: true,
                showToggle: false,
                showColumns: false,
                showExport: false,
                searchFormVisible: true,
                commonSearch: true,
                pageList: [10, 25, 50, 100],
                columns: [
                    [
                        {field: 'id', title: 'ID', operate: false},
                        /*{field: 'city_id', title: '城市', searchList: $.getJSON('general/lihua_city/sel'),
                            formatter: function (value, row, index) {return row.city_name;}},*/
                        {field: 'user.username', title: '用户', operate: false, formatter: function (value, row, index) {
                                return value + '<p class="text-orange">'+ row.user.mobile +'</p>';
                            }},
                        {field: 'user.mobile', title: '用户手机', visible: false},
                        {field: 'type', title: '红包类型', searchList: {"1":"新人红包","2":"满减红包","3":"无门槛红包","9":"分享红包"}, custom:{"1":"warning", "2":"info", "3":"success", "9":"danger"}, formatter: Table.api.formatter.label},
                        {field: 'name', title: '红包名称', operate: 'LIKE'},
                        {field: 'reach', title: '满', operate: false},
                        {field: 'reduce', title: '减', operate: false},
                        {field: 'start_date', title: '有效期', operate: false, formatter: function (value, row, index) {
                                return '从 ' + value + '<br>至 ' + row.end_date;
                            }},
                        {field: 'status', title: '状态', searchList: {"0":"未使用","1":"已使用","2":"已失效"}, custom:{"0":"success", "1":"danger", "2":"default"}, formatter: Table.api.formatter.label},
                        {field: 'order_sn', title: '订单号', operate: 'LIKE'},
                        {field: 'create_time', title: '创建时间', width: 90, operate: false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
            }
        }
    };
    return Controller;
});