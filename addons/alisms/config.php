<?php

return array (
  0 => 
  array (
    'name' => 'key',
    'title' => '应用key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'gXaUEpOkV3fWezJt',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'secret',
    'title' => '密钥secret',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'A3pSKHAY11Bj1DfHHOFk5Hecc5ujke',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'sign',
    'title' => '签名',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '丽华快餐',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'template',
    'title' => '短信模板',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      'register' => 'SMS_172743080',
      'login' => 'SMS_172743080',
    ),
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
