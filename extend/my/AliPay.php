<?php

namespace my;

require_once 'alipay/AopClient.php';

/**
 * 阿里支付
 */
class AliPay
{
    protected $config;
    public $err_msg = '';

    public function __construct($conf = [])
    {
        $config = [
            'gatewayUrl'         => 'https://openapi.alipay.com/gateway.do',
            'appId'              => '',//应用ID
            'signType'           => 'RSA2',
            'notifyUrl'          => '',//回调地址
            'rsaPrivateKey'      => '',//应用私钥
            'alipayrsaPublicKey' => '',//支付宝公钥
        ];
        $this->config = array_merge($config, $conf);
    }

    //通过此接口传入订单参数，同时唤起支付宝客户端。
    public function unifiedOrder($out_trade_no, $total_amount, $subject = '', $platform = 'app')
    {
        $aop = new \AopClient;
        $aop->gatewayUrl         = $this->config['gatewayUrl'];
        $aop->appId              = $this->config['appId'];
        $aop->signType           = $this->config['signType'];
        $aop->rsaPrivateKey      = $this->config['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->config['alipayrsaPublicKey'];

        $bizContent = [
            "timeout_express" => "30m",
            "total_amount"    => "$total_amount",
            "product_code"    => "",
            "subject"         => "$subject",
            "out_trade_no"    => "$out_trade_no"
        ];

        if ($platform == 'app') {
            require_once 'alipay/AlipayTradeAppPayRequest.php';
            $request = new \AlipayTradeAppPayRequest();
            $request->setNotifyUrl($this->config['notifyUrl']);
            $bizContent['product_code'] = 'QUICK_MSECURITY_PAY';
            $request->setBizContent(json_encode($bizContent));
            $result = $aop->sdkExecute($request);
            return ['result' => $result];

        } elseif ($platform == 'xcx') {
            require_once 'alipay/AlipayTradeCreateRequest.php';
            $request = new \AlipayTradeCreateRequest();
            $request->setNotifyUrl($this->config['notifyUrl']);
            $bizContent['product_code'] = 'FACE_TO_FACE_PAYMENT';
            $request->setBizContent(json_encode($bizContent));
            $result = $aop->execute($request);
            if ($result->alipay_trade_create_response->code == 10000) {
                return ['result' => $result->alipay_trade_create_response->trade_no];
            } else {
                if (isset($result->alipay_trade_create_response->sub_msg)) {
                    $this->err_msg = $result->alipay_trade_create_response->sub_msg;
                } else {
                    $this->err_msg = $result->alipay_trade_create_response->msg;
                }
            }
        } else {
            $this->err_msg = '平台错误';
        }

        return false;
    }

}